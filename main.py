import logging
from typing import Final, Type

from aiogram import types
from aiogram.contrib.fsm_storage.memory import MemoryStorage
from aiogram.dispatcher import FSMContext, filters
from aiogram.dispatcher.filters.state import State, StatesGroup

from services.bot.base import TelegramBotBase
from services.bot.handlers import BaseHandler, MessageHandler
from services.texts.start import start
from services.texts.stifler import (
    stifler_1,
    stifler_2,
    stifler_3,
    stifler_4,
    stifler_5,
    stifler_6,
    stifler_7,
    stifler_8,
    stifler_9,
    stifler_10,
)


API_TOKEN = ''
logging.basicConfig(level=logging.INFO)


class UserSatate(StatesGroup):
    start = State()
    stifler_1 = State()
    stifler_2 = State()
    stifler_3 = State()
    stifler_4 = State()
    stifler_5 = State()
    stifler_6 = State()
    stifler_7 = State()
    stifler_8 = State()
    stifler_9 = State()
    lose = State()
    win = State()


class StartHandler(MessageHandler):
    COMMANDS = ['game_start']

    async def handler(self, message: types.Message, state: FSMContext, **kwargs) -> None:
        await UserSatate.start.set()
        await message.answer(start)


class SetWordHandler(MessageHandler):
    CUSTOM_FILTERS = (
        filters.RegexpCommandsFilter([r'/set_word\s([A-z]|[А-я])+$']),
    )
    STATE = (
        UserSatate.start,
    )

    async def handler(self, message: types.Message, state: FSMContext, **kwargs) -> None:
        if not message.get_args():
            return

        await state.update_data(current_stifler=stifler_1)
        await message.answer(stifler_1)
        await UserSatate.stifler_1.set()

        current_word = message.get_args().split()[0]
        encrypted_word = self.get_encrypted_word(current_word)
        await message.answer(encrypted_word)
        await state.update_data(
            encrypted_word=encrypted_word,
            current_word=current_word,
        )

    def get_encrypted_word(self, current_word: str) -> str:
        return ' '.join(['_' for _ in current_word])


class LetterHandler(MessageHandler):
    REGEXP = r'^([A-z]|[А-я])$'
    STATE = (
        UserSatate.stifler_1,
        UserSatate.stifler_2,
        UserSatate.stifler_3,
        UserSatate.stifler_4,
        UserSatate.stifler_5,
        UserSatate.stifler_6,
        UserSatate.stifler_7,
        UserSatate.stifler_8,
        UserSatate.stifler_9,
    )

    async def handler(self, message: types.Message, state: FSMContext, **kwargs) -> None:
        async with state.proxy() as data:
            letter = message.text
            current_word = data['current_word']

            if not self.is_current_letter(letter, current_word):
                await UserSatate.next()

            encrypted_word = self.get_encrypted_word(letter, data)
            if self.is_won(current_word, encrypted_word):
                await UserSatate.win.set()

            current_state = await state.get_state()
            if current_state == 'UserSatate:lose':
                encrypted_word = ' '.join(list(current_word))

            current_stifler = await self.get_current_stifler(state)
            await state.update_data(current_stifler=current_stifler)
            await message.answer(current_stifler)
            await message.answer(encrypted_word)
            await state.update_data(encrypted_word=encrypted_word)

            await self.check_win_lose(message, state)

    async def check_win_lose(self, message: types.Message, state: FSMContext):
        current_state = await state.get_state()
        if current_state == 'UserSatate:win':
            await message.answer('Win')
            await state.finish()
        elif current_state == 'UserSatate:lose':
            await message.answer('Lose')
            await state.finish()

    async def get_current_stifler(self, state: FSMContext):
        current_state = await state.get_state()
        async with state.proxy() as data:
            return {
                'UserSatate:stifler_1': stifler_1,
                'UserSatate:stifler_2': stifler_2,
                'UserSatate:stifler_3': stifler_3,
                'UserSatate:stifler_4': stifler_4,
                'UserSatate:stifler_5': stifler_5,
                'UserSatate:stifler_6': stifler_6,
                'UserSatate:stifler_7': stifler_7,
                'UserSatate:stifler_8': stifler_8,
                'UserSatate:stifler_9': stifler_9,
                'UserSatate:lose': stifler_10,
            }.get(current_state, data['current_stifler'])

    def is_won(self, current_word: str, encrypted_word: str) -> bool:
        return current_word == ''.join(encrypted_word.split())

    def is_current_letter(self, letter: str, current_word: str) -> bool:
        return letter in current_word

    def get_encrypted_word(self, letter: str, data: dict) -> str:
        encrypted_word = []
        for word_letter in data['current_word']:
            if word_letter != letter and word_letter not in data['encrypted_word']:
                word_letter = '_'
            encrypted_word.append(word_letter)
        return ' '.join(encrypted_word)


class MyBot(TelegramBotBase):
    STORAGE = MemoryStorage()
    HANDLERS: Final[tuple[Type[BaseHandler]]] = (
        StartHandler,
        SetWordHandler,
        LetterHandler,
    )


if __name__ == '__main__':
    MyBot(API_TOKEN).start_bot(skip_updates=True)
