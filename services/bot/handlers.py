from typing import Callable, Optional, Pattern, Type, Union

from aiogram import Bot, filters
from aiogram.dispatcher.filters.state import State
from aiogram.utils.exceptions import TelegramAPIError


class BaseHandler:
    CUSTOM_FILTERS: Optional[list[Union[filters.Filter, Callable]]] = None
    RUN_TASK: bool = False

    def __init__(self, bot: Bot):
        self.bot = bot

    async def __call__(self, *args, **kwargs) -> None:
        await self.handler(*args, **kwargs)

    async def handler(self, *args, **kwargs) -> None:
        ...


class MessageHandler(BaseHandler):
    COMMANDS: Optional[tuple[str]] = None
    CONTENT_TYPES: Optional[tuple[str]] = None
    REGEXP: Optional[Pattern[str]] = None
    STATE: Optional[str] = None


class EditedMessageHandler(BaseHandler):
    COMMANDS: Optional[tuple[str]] = None
    CONTENT_TYPES: Optional[tuple[str]] = None
    REGEXP: Optional[Pattern[str]] = None
    STATE: Optional[str] = None


class InlineHandler(BaseHandler):
    STATE: Optional[str] = None


class ChosenInlineHandler(BaseHandler):
    STATE: Optional[str] = None


class CallbackQueryHandler(BaseHandler):
    STATE: Optional[str] = None


class ShippingQueryHandler(BaseHandler):
    STATE: Optional[str] = None


class PreCheckoutQueryHandler(BaseHandler):
    STATE: Optional[str] = None


class PollHandler(BaseHandler):
    ...


class PollAnswerHandler(BaseHandler):
    ...


class MyChatMemberHandler(BaseHandler):
    ...


class ChatMemberHandler(BaseHandler):
    ...


class ChatJoinRequestHandler(BaseHandler):
    ...


class ErrorsHandler(BaseHandler):
    EXCEPTION: Optional[Type[TelegramAPIError]] = None
