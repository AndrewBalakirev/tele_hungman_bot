import inspect
from typing import Callable, Final, Type, Optional

from aiogram import Bot, Dispatcher, executor

from .handlers import (
    BaseHandler,
    CallbackQueryHandler,
    ChatJoinRequestHandler,
    ChatMemberHandler,
    ChosenInlineHandler,
    EditedMessageHandler,
    ErrorsHandler,
    InlineHandler,
    MessageHandler,
    MyChatMemberHandler,
    PollAnswerHandler,
    PollHandler,
    PreCheckoutQueryHandler,
    ShippingQueryHandler,
)


class TelegramBotBase:
    STORAGE = None
    HANDLERS: Final[tuple[Type[BaseHandler]]] = ()

    def __init__(self, api_key: str):
        self._api_key = api_key
        self.bot = Bot(token=self._api_key)
        self.dp = Dispatcher(
            self.bot,
            storage=self.STORAGE,
        )

    def start_bot(self, **kwargs) -> None:
        kwargs['on_startup'] = self._on_startup
        kwargs['on_shutdown'] = self._on_shutdown
        executor.start_polling(self.dp, **kwargs)

    async def on_startup(self, url: Optional[str], cert: Optional[str]) -> None:
        ...

    async def on_shutdown(self):
        ...

    async def _on_startup(
        self,
        dp: Dispatcher,
        url: Optional[str] = None,
        cert: Optional[str] = None,
    ) -> None:

        self.__register_handlers()
        await self.on_startup(url, cert)

    async def _on_shutdown(self, dp: Dispatcher) -> None:
        await self.on_shutdown()

    def __register_handlers(self):
        for handler_class in self.HANDLERS:
            handler = handler_class(self.bot)
            registers = self._get_handler_registers(handler)
            for register in registers:
                register(handler)

    def _get_handler_registers(self, handler: BaseHandler) -> list[Callable]:
        registers = []
        registers_map: Final[dict[Type[BaseHandler], Callable]] = {
            MessageHandler: self.__register_message_handler,
            EditedMessageHandler: self.__register_edited_message_handler,
            InlineHandler: self.__register_inline_handler,
            ChosenInlineHandler: self.__register_chosen_inline_handler,
            CallbackQueryHandler: self.__register_callback_query_handler,
            ShippingQueryHandler: self.__register_shipping_query_handler,
            PreCheckoutQueryHandler: self.__register_pre_checkout_query_handler,
            PollHandler: self.__register_poll_handler,
            PollAnswerHandler: self.__register_poll_answer_handler,
            MyChatMemberHandler: self.__register_my_chat_member_handler,
            ChatMemberHandler: self.__register_chat_member_handler,
            ChatJoinRequestHandler: self.__register_chat_join_request_handler,
            ErrorsHandler: self.__register_errors_handler,
        }
        for handler_class, register in registers_map.items():
            if isinstance(handler, handler_class):
                registers.append(register)
        return registers

    def __register_message_handler(self, handler: MessageHandler) -> None:
        self.dp.register_message_handler(
            handler,
            *self.__get_handler_custom_filters(handler),
            regexp=handler.REGEXP,
            commands=handler.COMMANDS,
            state=handler.STATE,
            run_task=handler.RUN_TASK,
        )

    def __register_edited_message_handler(self, handler: EditedMessageHandler) -> None:
        self.dp.register_edited_message_handler(
            handler,
            *self.__get_handler_custom_filters(handler),
            regexp=handler.REGEXP,
            commands=handler.COMMANDS,
            state=handler.STATE,
            run_task=handler.RUN_TASK,
        )

    def __register_inline_handler(self, handler: InlineHandler) -> None:
        self.dp.register_inline_handler(
            handler,
            *self.__get_handler_custom_filters(handler),
            state=handler.STATE,
            run_task=handler.RUN_TASK,
        )

    def __register_chosen_inline_handler(self, handler: ChosenInlineHandler) -> None:
        self.dp.register_chosen_inline_handler(
            handler,
            *self.__get_handler_custom_filters(handler),
            state=handler.STATE,
            run_task=handler.RUN_TASK,
        )

    def __register_callback_query_handler(self, handler: CallbackQueryHandler) -> None:
        self.dp.register_callback_query_handler(
            handler,
            *self.__get_handler_custom_filters(handler),
            state=handler.STATE,
            run_task=handler.RUN_TASK,
        )

    def __register_shipping_query_handler(self, handler: ShippingQueryHandler) -> None:
        self.dp.register_shipping_query_handler(
            handler,
            *self.__get_handler_custom_filters(handler),
            state=handler.STATE,
            run_task=handler.RUN_TASK,
        )

    def __register_pre_checkout_query_handler(self, handler: PreCheckoutQueryHandler) -> None:
        self.dp.register_pre_checkout_query_handler(
            handler,
            *self.__get_handler_custom_filters(handler),
            state=handler.STATE,
            run_task=handler.RUN_TASK,
        )

    def __register_poll_handler(self, handler: PollHandler) -> None:
        self.dp.register_poll_handler(
            handler,
            *self.__get_handler_custom_filters(handler),
            run_task=handler.RUN_TASK,
        )

    def __register_poll_answer_handler(self, handler: PollAnswerHandler) -> None:
        self.dp.register_poll_answer_handler(
            handler,
            *self.__get_handler_custom_filters(handler),
            run_task=handler.RUN_TASK,
        )

    def __register_my_chat_member_handler(self, handler: MyChatMemberHandler) -> None:
        self.dp.register_my_chat_member_handler(
            handler,
            *self.__get_handler_custom_filters(handler),
            run_task=handler.RUN_TASK,
        )

    def __register_chat_member_handler(self, handler: ChatMemberHandler) -> None:
        self.dp.register_chat_member_handler(
            handler,
            *self.__get_handler_custom_filters(handler),
            run_task=handler.RUN_TASK,
        )

    def __register_chat_join_request_handler(self, handler: ChatJoinRequestHandler) -> None:
        self.dp.register_chat_join_request_handler(
            handler,
            *self.__get_handler_custom_filters(handler),
            run_task=handler.RUN_TASK,
        )

    def __register_errors_handler(self, handler: ErrorsHandler) -> None:
        self.dp.register_errors_handler(
            handler,
            *self.__get_handler_custom_filters(handler),
            exception=handler.EXCEPTION,
            run_task=handler.RUN_TASK,
        )

    @staticmethod
    def __get_handler_custom_filters(handler: BaseHandler) -> list[Callable]:
        custom_filters = handler.CUSTOM_FILTERS or []
        methods: tuple[str, Callable] = inspect.getmembers(handler, predicate=inspect.ismethod)

        for name, method in methods:
            if name.startswith('custom_filter_'):
                custom_filters.append(method)

        return custom_filters
